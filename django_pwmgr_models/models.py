from datetime import datetime

from django.conf import settings

from django_mdat_customer.django_mdat_customer.models import *


class PwmgrTenantSettings(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, unique=True)
    user_pw_log_enabled = models.BooleanField(default=True)

    class Meta:
        db_table = "pwmgr_tenant_settings"


class PwmgrDbs(models.Model):
    group = models.ForeignKey("PwmgrGroups", models.DO_NOTHING, db_column="group")
    db_type = models.ForeignKey("PwmgrDbtypes", models.DO_NOTHING, db_column="dbtype")
    git_repo_url = models.CharField(max_length=250, blank=False, null=False)
    db_master_pw = models.CharField(max_length=50, blank=False, null=False)
    db_created = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "pwmgr_dbs"


class PwmgrDbtypes(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        db_table = "pwmgr_dbtypes"


class PwmgrGroups(models.Model):
    group_name = models.CharField(max_length=50, blank=True, null=True)
    user = models.ManyToManyField(settings.AUTH_USER_MODEL)
    group_created = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "pwmgr_groups"
